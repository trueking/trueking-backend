package com.trueking.business.service;

import com.trueking.business.model.ItemDTO;

public interface ItemBS {
	public abstract void create(ItemDTO item);
	//public abstract void recover(ItemDTOQuery query);
	public abstract void update(ItemDTO item);
	public abstract void delete(ItemDTO item);
	public abstract void get(Integer id);
}
