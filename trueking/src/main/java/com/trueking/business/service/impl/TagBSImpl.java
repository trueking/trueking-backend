package com.trueking.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trueking.business.model.TagDTO;
import com.trueking.business.repository.TagRepository;
import com.trueking.business.service.TagBS;

@Service
public class TagBSImpl implements TagBS {

	@Autowired
	TagRepository tagRepository;
	
	@Override
	public void create(TagDTO item) {
		//TODO: prevalidations
		tagRepository.save(item);
	}

	@Override
	public void update(TagDTO item) {
		//TODO: prevalidations
		tagRepository.save(item);
		
	}

	@Override
	public void delete(TagDTO item) {
		tagRepository.delete(item.getId());
		
	}

	@Override
	public void get(Integer id) {
		tagRepository.getOne(id);
	}

}
