package com.trueking.business.service;

import com.trueking.business.model.TagDTO;

public interface TagBS {
	public abstract void create(TagDTO item);
	//public abstract void recover(ItemDTOQuery query);
	public abstract void update(TagDTO item);
	public abstract void delete(TagDTO item);
	public abstract void get(Integer id);
}
