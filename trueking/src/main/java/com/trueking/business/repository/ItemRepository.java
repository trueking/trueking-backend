package com.trueking.business.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trueking.business.model.ItemDTO;

public interface ItemRepository extends	JpaRepository<ItemDTO, Serializable>{

}
