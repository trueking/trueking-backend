package com.trueking.business.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trueking.business.model.TagDTO;

@Transactional
public interface TagRepository extends	JpaRepository<TagDTO, Integer>{

}
