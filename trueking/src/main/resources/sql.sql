--DROP SCHEMA trueking CASCADE;
CREATE SCHEMA trueking;
SET SCHEMA 'trueking';

CREATE TABLE Tag ( 
	id integer NOT NULL,
	nameTag text NOT NULL,
	CONSTRAINT PKeyTag PRIMARY KEY (id)
);

CREATE TABLE TagxTag (
	idTag1 integer NOT NULL,
	idTag2 integer NOT NULL,
	CONSTRAINT uniquePair UNIQUE (idTag1, idTag2),
	CONSTRAINT notWithSelf CHECK (idTag1 <> idTag2),
	CONSTRAINT FKTag1 FOREIGN KEY (idTag1) REFERENCES Tag(id),
	CONSTRAINT FKTag2 FOREIGN KEY (idTag2) REFERENCES Tag(id)
);

CREATE TABLE Item (
	id integer NOT NULL,
	nameItem text NOT NULL,
	description text,
	idOwner integer NOT NULL,
	CONSTRAINT PKeyItem PRIMARY KEY (id)
);

CREATE TABLE ItemxImg (
	id integer NOT NULL,
	img text,
	CONSTRAINT FKTag FOREIGN KEY (id) REFERENCES Tag(id)
);

CREATE TABLE ItemxTag (
	idItem integer NOT NULL,
	idTag integer NOT NULL,
	CONSTRAINT FKTagx FOREIGN KEY (idTag) REFERENCES Tag(id),
	CONSTRAINT FKItem FOREIGN KEY (idItem) REFERENCES Item(id)
);

SELECT * FROM tag;
SELECT * FROM tag t WHERE t.id = 'idExample';
SELECT * FROM tag t WHERE t.nameTag = 'nameTagExample';

SELECT * FROM Item;
SELECT * FROM Item i WHERE i.id = 'idExample';
SELECT * FROM Item i WHERE i.nameItem = 'nameItemExample';
SELECT * FROM Item i WHERE i.idPropietari = 'idOwner';

SELECT tt.idTag2 FROM TagxTag tt WHERE tt.idTag1 = 'idExample';
